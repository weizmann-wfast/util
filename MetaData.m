classdef MetaData < handle
   
    properties
    
        list = util.MetaProperty.empty;
        classname;     
        
        deflate=1;

        debug_bit = 0;
        
    end
    
    properties(Hidden=true)
        
        prev_classnames;
        chunk = [64 64];
        saving_lock = 0;
        version = 2.01;
        
    end
    
    methods % constructor
       
        function obj = MetaData(other)
            
            if nargin>0 && isa(other, 'util.MetaData')
                
                obj.list = other.list;
                obj.classname = other.classname; 
                obj.deflate = other.deflate;
                obj.debug_bit = other.debug_bit;
                obj.chunk = other.chunk;
                obj.version = other.version;
                
            elseif nargin>0 && isa(other, 'MetaData')
                error('wrong type of MetaData!!!');                
            else
                
                if obj.debug_bit, disp(['MetaData constructor v' num2str(obj.version)]); end
                
            end
            
        end
       
    end
    
    methods % getters
       
        function [prop, index] = getProperty(obj, name)
             
            check = 0;
            index = 0;
            
            for ii = 1:length(obj.list)
               
                if strcmp(name, obj.list(ii).name)
                    prop = obj.list(ii);
                    index = ii;
                    check = 1;
                    return;
                end
                
            end
            
            for ii = 1:length(obj.list)
                for jj = 1:length(obj.list(ii).alias)
                    if strcmp(name, obj.list(ii).alias{jj})
                        prop = obj.list(ii);
                        index = ii;
                        check = 1;
                        return;
                    end
                end
            end
            
            if check==0
                if obj.debug_bit, disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']); end
                prop = util.MetaProperty.empty;
            end
            
        end
        
        function getPropertyNames(obj)
            
        end
        
        function getPropertyValues(obj)
            
        end
        
        function t = textheader(obj)
           
            t = 'CLASS LOG FOR: '; 
            
        end
        
    end
    
    methods % set methods
       
        function setup(obj, class_obj)
           
            obj.list = util.MetaProperty.empty;

            mc = metaclass(class_obj);
           
            obj.classname = mc.Name;
            
            pl = mc.PropertyList;
                        
            for ii = 1:length(pl)
             
                name = (pl(ii).Name);
                if isa(class_obj.(name), 'util.MetaData') % || pl(ii).Dependent 
                    continue;
                end
                
                if isobject(class_obj.(name))
                    type = 'object';
                elseif (numel(class_obj.(name))==0 || numel(class_obj.(name))>1) && ~ischar(class_obj.(name))
                    type = 'data';
                else
                    type = 'attribute';
                end
                
                if pl(ii).HasDefault
                    obj.list(end+1) = util.MetaProperty(name, type, {}, pl(ii).DefaultValue, pl(ii).Hidden, pl(ii).Dependent, pl(ii).Transient); % copyable is set by default to 1
                else
                    obj.list(end+1) = util.MetaProperty(name, type, {}, [], pl(ii).Hidden, pl(ii).Dependent, pl(ii).Transient); % copyable is set by default to 1
                end
                
            end
            
        end
            
        function setProperty(obj, name, type, alias, default_value, hidden, dependent, transient, copyable)
  
            check = 0;
            
            for ii = 1:length(obj.list)
               
                if strcmp(name, obj.list(ii).name)
                   
                    % whatever you don't input, the default values for MetaProperty gets used
                    if nargin>=3 && ~isempty(type), obj.list(ii).type = type; end
                    if nargin>=4 && ~isempty(alias), obj.list(ii).alias = alias; end
                    if nargin>=5 && ~isempty(default_value), obj.list(ii).default_value = default_value; end
                    if nargin>=6 && ~isempty(hidden), obj.list(ii).hidden = hidden; end
                    if nargin>=7 && ~isempty(dependent), obj.list(ii).dependent = dependent; end
                    if nargin>=8 && ~isempty(transient), obj.list(ii).transient = transient; end
                    if nargin>=9 && ~isempty(copyable), obj.list(ii).copyable = copyable; end
                    check = 1;
                    break;
                    
                end
                
            end
            
            if check==0
                disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
            end
            
        end
        
        function setType(obj, name_list, type)
            
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
            
            for ii = 1:length(name_list)
                
                name = name_list{ii};
                
                check = 0;

                for jj = 1:length(obj.list)

                    if strcmp(name, obj.list(jj).name)
                        obj.list(jj).type = type;
                        check = 1;
                        break;
                    end

                end

                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
            
            end
                
        end
                
        function addAlias(obj, name, alias)
            
            check = 0;
              
            for ii = 1:length(obj.list)
               
                if strcmp(name, obj.list(ii).name)
                    
                    if isempty(obj.list(ii).alias)
                        obj.list(ii).alias = util.vec.tocell(alias);
                    else
                        obj.list(ii).alias = {obj.list(ii).alias alias}; % horzcat
                    end
                    
                    check = 1;
                    break;
                    
                end
                
            end
            
            if check==0
                disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
            end
                        
        end
        
        function clearAlias(obj, name)
                       
            check = 0;
              
            for ii = 1:length(obj.list)
               
                if strcmp(name, obj.list(ii).name)
                                       
                    obj.list(ii).alias = {};
                                       
                    check = 1;
                    break;
                    
                end
                
            end
            
            if check==0
                disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
            end 
            
        end
        
        function setHidden(obj, name_list, hidden)
                    
            if nargin<3 || isempty(hidden)
                hidden = 1;
            end
                     
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
            
            for ii = 1:length(name_list)
                
                name = name_list{ii};
                
                check = 0;
                
                for jj = 1:length(obj.list)
                    
                    if strcmp(name, obj.list(jj).name)
                        
                        obj.list(jj).hidden = hidden;
                        check = 1;
                        break;
                        
                    end
                    
                end
                
                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
                
            end
            
        end
        
        function setDependent(obj, name_list, dependent)
                    
            if nargin<3 || isempty(dependent)
                dependent = 1;
            end
                                    
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
            
            for ii = 1:length(name_list)
                
                name = name_list{ii};
            
                check = 0;
                
                for jj = 1:length(obj.list)
                    
                    if strcmp(name, obj.list(jj).name)
                        
                        obj.list(jj).dependent = dependent;
                        check = 1;
                        break;
                        
                    end
                    
                end
                
                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
                
            end
            
        end
                
        function setTransient(obj, name_list, transient)
                    
            if nargin<3 || isempty(transient)
                transient = 1;
            end
                                    
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
                          
            for ii = 1:length(name_list)
            
                name = name_list{ii};
                
                check = 0;
                
                for jj = 1:length(obj.list)
                    
                    if strcmp(name, obj.list(jj).name)
                        
                        obj.list(jj).transient = transient;
                        check = 1;
                        break;
                        
                    end
                    
                end
                
                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
            
            end
            
        end
        
        function setCopyable(obj, name_list, copyable)
                    
            if nargin<3 || isempty(copyable)
                copyable = 1;
            end
                        
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
            
            for ii = 1:length(name_list)
                
                name = name_list{ii};
            
                check = 0;
                
                for jj = 1:length(obj.list)
                    
                    if strcmp(name, obj.list(jj).name)
                        
                        obj.list(jj).copyable = copyable;
                        check = 1;
                        break;
                        
                    end
                    
                end
                
                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
            
            end
                
        end
        
        function setExternal(obj, name_list, external)
                    
            if nargin<3 || isempty(external)
                external = 1;
            end
                     
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
            
            for ii = 1:length(name_list)
            
                name = name_list{ii};
                
                check = 0;
                
                for jj = 1:length(obj.list)
                    
                    if strcmp(name, obj.list(jj).name)
                        
                        obj.list(jj).external = external;
                        check = 1;
                        break;
                        
                    end
                    
                end
                
                if check==0
                    disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
                end
                
            end
            
        end
        
        function setMetaProperties(obj, name_list, hidden, dependent, transient, copyable, external)
                                   
            if ischar(name_list)
                name_list = {name_list};
            end
            
            if ~iscellstr(name_list)
                error('Please give a cell array of names or a single name');
            end
                        
            for jj = 1:length(obj.list)
            
                name = name_list{ii};
                
                check = 0;
                
                if strcmp(name, obj.list(jj).name)
                    
                    % whatever you don't input, the default values for MetaProperty gets used
                    if nargin>=3 && ~isempty(hidden), obj.list(jj).hidden = hidden; end
                    if nargin>=4 && ~isempty(dependent), obj.list(jj).dependent = dependent; end
                    if nargin>=5 && ~isempty(transient), obj.list(jj).transient = transient; end
                    if nargin>=6 && ~isempty(copyable), obj.list(jj).copyable = copyable; end
                    if nargin>=7 && ~isempty(external), obj.list(jj).external = external; end

                    check = 1;
                    break;
                    
                end
                
            end
            
            if check==0
                disp(['Warning: no property named ' name ' is found in ' obj.classname ' class']);
            end
                        
        end

        function setAttribute(obj, name_list)
           
            obj.setType(name_list, 'attribute');
            
        end
               
        function setAllAttributes(obj)
            
            for ii = 1:length(obj.list)
               
                if ~util.text.cs(obj.list(ii).type, 'object')
                    obj.list(ii).type = 'attribute';
                end
                
            end
            
        end
        
    end
    
    methods % save to file / read from file
        
        function saveHDF5(obj, class_obj, filename, root_location, if_exist)
                    
            import util.text.cs;
            
            if obj.saving_lock
                return;
            end
            
            if obj.debug_bit, disp(['saving ' obj.classname ' to "' filename '"']); end
            
            if nargin<4 || isempty(root_location)
                root_location = '';
            end
        
            if nargin<5 || isempty(if_exist)
                if_exist = 'overwrite';
            end
            
            if exist(filename, 'file') && cs(if_exist,'overwrite') % just destroy the file if it exists
                if obj.debug_bit, disp(['Warning: overwriting file: ' filename]); end
                delete(filename);
            elseif exist(filename, 'file') && ~cs(if_exist,'append') % can't overwrite, can't append--> gives error
                error(['Cannot overwrite existing file: ' filename]);
            end
            
            if ~exist(filename, 'file') % in any case we got this far with no file --> create one
                h5create(filename, '/null', 1);
            elseif ~strcmp(obj.addSlash(root_location), '/')
                fid = H5F.open(filename,'H5F_ACC_RDWR', 'H5P_DEFAULT');
                gid = H5G.create(fid, obj.remSlash(root_location),25);
            end

            f_path = fileparts(filename);
            
            [success, message] = mkdir(f_path);
            
            if success~=1
                disp(['error making a directory: ' message]);
            end
            
            h5writeatt(filename, obj.addSlash(root_location), 'class', obj.classname); % add an attribute with the class of the object... 
            
            % go over the data-type properties
            for ii = 1:length(obj.list)
                
                try
                
                    if ~obj.list(ii).transient && ~builtin('isempty', class_obj.(obj.list(ii).name)) && cs(obj.list(ii).type, 'data')

                        name = obj.list(ii).name;
                        location = [obj.addSlash(root_location), name];
                        chunk = obj.chunk;

                        if obj.debug_bit>=5, disp(['name= ' name ' (type: data, location= ' location ')']); end

                        data = class_obj.(name);

                        if ~iscell(data)

                            data_size = size(data);            
                            for kk = 1:length(data_size), if data_size(kk)<1, data_size(kk)=1; end; end
                            datatype = class(data);                       
                            if strcmp(datatype,'logical')
                                datatype = 'uint8';
                            end

                            % make sure the chunk size is never bigger than the data size
                            if length(chunk)>length(data_size)
                                chunk = chunk(1:length(data_size));
                            else
                                chunk = [chunk ones(1,length(data_size)-length(chunk))];
                            end

                            for kk = 1:length(data_size)
                                if data_size(kk)<chunk(kk)
                                   chunk(kk) = data_size(kk);
                                end
                            end

                            % check the data type is allowed in hdf5
                            if cs(datatype, 'char')
                                disp(['char property "' name '" is classified as data. saving as attribute...']);  
                                continue;
                            elseif ~cs(datatype, {'int8','uint8','int16','uint16','int32','uint32','int64','uint64','single','double'})
                                continue;
                            end

                            if isempty(obj.deflate)
                                h5create(filename, location, data_size, 'DataType', datatype, 'ChunkSize', chunk);
                            else                            
                                h5create(filename, location, data_size, 'DataType', datatype, 'ChunkSize', chunk, 'Deflate', obj.deflate);
                            end

                            if isa(data,'logical')
                                data = uint8(data);
                            end

                            h5write(filename, location, data);

                        else % if it is a cell of data

                            for jj = 1:length(data)

                                data_size = size(data{jj});
                                for kk = 1:length(data_size), if data_size(kk)<1, data_size(kk)=1; end; end
                                datatype = class(data{jj});

                                if strcmp(datatype, 'logical')
                                    datatype = 'uint8';
                                end

                                if length(chunk)>length(data_size)
                                    chunk = chunk(1:length(data_size));
                                else
                                    chunk = [chunk ones(1,length(data_size)-length(chunk))];
                                end

                                for kk = 1:length(data_size)
                                    if data_size(kk)<chunk(kk)
                                        chunk(kk) = data_size(kk);
                                    end
                                end

                                % check the data type is allowed in hdf5
                                if ~cs(datatype, {'int8','uint8','int16','uint16','int32','uint32','int64','uint64','single','double'})
                                    continue;
                                end
                                
                                if length(data)>99
                                    loc_num = sprintf('%s{%03d}', location, jj);
                                elseif length(data)>9
                                    loc_num = sprintf('%s{%02d}', location, jj);
                                else
                                    loc_num = sprintf('%s{%d}', location, jj);
                                end

                                if isempty(obj.deflate)
                                    h5create(filename, loc_num, data_size, 'DataType', datatype, 'ChunkSize', chunk);
                                else
                                    h5create(filename, loc_num, data_size, 'DataType', datatype, 'ChunkSize', chunk, 'Deflate', obj.deflate);
                                end

                                if isa(data{jj},'logical')
                                    data{jj} = uint8(data{jj});
                                end

                                if ~isempty(data{jj}), h5write(filename, loc_num, data{jj}); end

                            end
                            
                        end

                    end
                    
                catch ME
                    error('Problem while saving %s in dataset: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport); 
                end
                
            end % going over data-type properties
            
            % go over the attribute-type properties
            for ii = 1:length(obj.list)
               
                try 
                    
                    if ~obj.list(ii).transient && ~builtin('isempty', class_obj.(obj.list(ii).name)) && (cs(obj.list(ii).type, 'attribute') || ischar(class_obj.(obj.list(ii).name)))

                        name = obj.list(ii).name;
                        location = obj.addSlash(root_location);

                        if obj.debug_bit>=5, disp(['name= ' name ' (type: attribute, location= ' location ')']); end
                        val = class_obj.(name);

                        if ~iscell(val) % if the value is not a cell, just write it
                            if islogical(val), val = uint8(val); end
                            h5writeatt(filename, location, name, val)
                        else % if it is a cell, write each component in a sub-attribute
                            for jj = 1:length(val)

                                if islogical(val{jj}), val{jj} = uint8(val{jj}); end
                                h5writeatt(filename, location, sprintf('%s{%d}', name, jj), val{jj});

                            end

                        end
                    end
                
                catch ME
                    error('Problem while saving %s in attribute: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport);
                end
            
            end % going over attributes
                
            obj.saving_lock = 1; % if sub object has pointer back to this, saving lock will prevent a loop
            
            % go over the other objects embedded
            for ii = 1:length(obj.list)
                
                try
                
                    if ~obj.list(ii).transient && ~obj.list(ii).dependent && ~builtin('isempty', class_obj.(obj.list(ii).name)) && cs(obj.list(ii).type, 'object') % dependent OBJECTS are not saved!

                        sub_object = class_obj.(obj.list(ii).name); % this is the object that needs to be saved...

                        if isa(sub_object, 'datetime'), continue; end

                        mc = metaclass(sub_object);
                        pl = mc.PropertyList; % the sub_object's property list...

                        if obj.debug_bit>=5, disp(['obj name= ' sub_object ' location= ' location]); end

                        check = 0;

                        for jj = 1:length(pl) % go over this class to look for MetaData object...

                            if isa(sub_object(1).(pl(jj).Name),'util.MetaData')

                                if length(sub_object)==1
                                    location = [obj.addSlash(root_location) obj.list(ii).name];
                                    sub_object.(pl(jj).Name).saveHDF5(sub_object, filename, location, 'append'); % write data in heirarchy, don't overwrite!
                                else % save an array of objects...
                                    for kk = 1:length(sub_object)
                                        if length(sub_object)>99
                                            location = sprintf('%s%s(%03d)', obj.addSlash(root_location), obj.list(ii).name, kk); % write the elements of the sub_object vector
                                        elseif length(sub_object)>9
                                            location = sprintf('%s%s(%02d)', obj.addSlash(root_location), obj.list(ii).name, kk); % write the elements of the sub_object vector
                                        else
                                            location = sprintf('%s%s(%d)', obj.addSlash(root_location), obj.list(ii).name, kk); % write the elements of the sub_object vector
                                        end
                                        sub_object(kk).(pl(jj).Name).saveHDF5(sub_object(kk), filename, location, 'append'); % write data in heirarchy, don't overwrite!
                                    end
                                end

                                check = 1;
                                break;

                            end

                        end

                        if check==0
                            if obj.debug_bit, disp(['Warning: sub object "' obj.list(ii).name '" does not have a MetaData object and cannot be saved']); end
                        end
                    end
                    
                catch ME
                    error('Problem while saving %s in object: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport); 
                end
                
            end % going over objects
            
            obj.saving_lock = 0;
            
            if exist('fid', 'var'), H5F.close(fid); end
            if exist('gid', 'var'), H5G.close(gid); end
            
        end
        
        function loadHDF5(obj, class_obj, filename, root_location)
            
            import util.text.cs;
            
%             disp('-----> loading metadata now <-----');
            
            if nargin<4 || isempty(root_location)
                root_location = '/';
            end
                        
            if obj.debug_bit, disp(['loading HDF5 for ' obj.classname ', goodluck!']); end
            
            info = h5info(filename, obj.addSlash(root_location));
            
            for ii = 1:length(obj.list) % go over all properties in this class

                if obj.list(ii).transient || obj.list(ii).dependent || obj.list(ii).external % skip these properties.
                    if obj.debug_bit>=10, disp(['skipping: ' obj.list(ii).name]); end
                    continue;
                end
                                
                if obj.debug_bit, disp(['name= ' obj.list(ii).name ' (location= ' root_location ')']); end
                
                if cs(obj.list(ii).type, 'data')
                    
                    try
                    
                        if obj.debug_bit>=10, disp(['loading data for ' obj.list(ii).name]); end
                        
                        data = obj.checkForData(info, obj.list(ii));
                        if isempty(data), data = obj.checkForAttribute(info, obj.list(ii));end

                        if ~isempty(data), class_obj.(obj.list(ii).name) = data; end

                        if obj.debug_bit>=10, disp(['data ' obj.list(ii).name ' was read with size= ' num2str(size(data))]); end
                    catch ME
                        error('Problem while loading %s in dataset: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport); 
                    end
                    
                end
                                
                if cs(obj.list(ii).type, 'attribute')
                   
                    try 
                        if obj.debug_bit>=10, disp(['loading attribute: ' obj.list(ii).name]); end

                        data = obj.checkForAttribute(info, obj.list(ii), root_location);
                        if obj.debug_bit>=10 && ischar(data), disp(['attribute: ' data]); end
                        if obj.debug_bit>=10 && isnumeric(data), disp(['attribute: ' num2str(util.vec.torow(data))]); end
                        if isempty(data), data = obj.checkForAttribute(info, obj.list(ii));end

                        if ~isempty(data), class_obj.(obj.list(ii).name) = data; end

                        if obj.debug_bit>=10, disp(['attribute ' obj.list(ii).name ' was read: ' util.vec.torow(data)]); end
                    catch ME
                        error('Problem while loading %s in attribute: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport); 
                    end
                    
                end
                
                if cs(obj.list(ii).type, 'object')
                                       
                    % recursively (add new objects and) read the sub groups
                    % into each object on the list
                    try
                        
                        if obj.debug_bit>=10, disp(['looking for object (subgroup) ' obj.list(ii).name]); end

                        if isfield(info, 'Groups')
                            groups = info.Groups;
                        else
                            groups = [];
                        end
                        
                        for jj = 1:length(groups)
                            
                            group_name = groups(jj).Name;
                            [~, name] = fileparts(group_name); 
                            idx = obj.array_index(name, obj.list(ii).name);
                            
                            if isempty(idx)
                                continue;
                            else
                                if idx==0
                                    idx = 1;
                                end
                                
                                ctor = str2func(class(class_obj.(obj.list(ii).name))); % create a function handle to the constructor
                                class_obj.(obj.list(ii).name)(idx) = ctor();
                                
                                meta_name = obj.getMetaName(class_obj.(obj.list(ii).name));
                                if ~isempty(meta_name)
                                    if obj.debug_bit>=10, disp(['calling the sub-object ' obj.list(ii).name]); end
                                    class_obj.(obj.list(ii).name)(idx).(meta_name).loadHDF5(class_obj.(obj.list(ii).name)(idx), filename, group_name); % call the sub object's metadata load function
                                end
                            end
                            
                        end 
                        
%                         groups = obj.checkForSubGroup(info, obj.list(ii));
% 
%                         for jj = 1:length(groups)
% 
%                             try
%                                 if jj>length(class_obj.(obj.list(ii).name))
% 
%                                     a = str2func(class(class_obj.(obj.list(ii).name))); % create a function handle to the constructor
% 
%                                     class_obj.(obj.list(ii).name)(jj) = a();
% 
%                                 end
% 
%                                 meta_name = obj.getMetaName(class_obj.(obj.list(ii).name));
%                                 if ~isempty(meta_name)
%                                     if obj.debug_bit>=10, disp(['calling the subgroup ' obj.list(ii).name]); end
%                                     class_obj.(obj.list(ii).name)(jj).(meta_name).loadHDF5(class_obj.(obj.list(ii).name)(jj), filename, groups{jj}); % call the sub object's metadata load function
%                                 end
% 
%                             catch ME
%                                 warning(ME.getReport);
%                             end
% 
%                         end

                    catch ME
                        error('Problem while loading %s in object: "%s".\n %s', obj.classname, obj.list(ii).name, ME.getReport); 
                    end
                        
                end
                   
            end
            
        end
        
        function data = checkForData(obj, info, prop)
            
            data = {};
            if isfield(info, 'Datasets')
                
                for ii = 1:length(info.Datasets)

                    name = prop.name;

                    for jj = 1:length(prop.alias)+1 % plus one for the primary name

                        if jj>1, name = prop.alias{jj-1}; end

%                         if strcmp(name, info.Datasets(ii).Name)
% 
%                             data = h5read(info.Filename, [info.Name '/' name]);
%                             return;
% 
%                         end
% 
%                         if ~isempty(regexp(info.Datasets(ii).Name, [name '(_\d+)+$'], 'once'))
%                         match_idx = regexp(info.Datasets(ii).Name, '(\d+)+$', 'once');
%                         num = str2double(info.Datasets(ii).Name(match_idx:end));
%                         data{num} = h5read(info.Filename, [info.Name '/' info.Datasets(ii).Name]);
                        
                        idx = obj.cell_index(info.Datasets(ii).Name, name);
                        if isempty(idx) 
                            continue;
                        elseif idx==0
                            data = h5read(info.Filename, [info.Name '/' info.Datasets(ii).Name]);
                        elseif idx>0
                            data{idx} = h5read(info.Filename, [info.Name '/' info.Datasets(ii).Name]);
                        end
                        
                    end

                end

            end
                            
        end
              
        function data = checkForAttribute(obj, info, prop, root_location)
            
            data = [];
            
            name = prop.name;
            found = 0;
            for jj = 1:length(prop.alias)+1 % plus one for the primary name

                if jj>1, name = prop.alias{jj-1}; end
                
                for ii = 1:length(info.Attributes)
                    
%                     if ~isempty(regexp(info.Attributes(ii).Name, [name '(_\d+)+$'], 'once')) % if there is a "_" and some digits at the end of the attribute name 
%                         match_idx = regexp(info.Attributes(ii).Name, '(\d+)+$', 'once');
%                         num = str2double(info.Attributes(ii).Name(match_idx:end));
                       
                    idx = obj.cell_index(info.Attributes(ii).Name, name);
                    
                    if ~isempty(idx) && idx>0
                        
                        try 
                            data{idx} = h5readatt(info.Filename, info.Name, info.Attributes(ii).Name);
                            if iscell(data{idx})
                                data{idx} = data{idx}{1}; % why do we need this??
                            end
                            found = 1;
                        catch
                            if obj.debug_bit, disp(['failed to load attribute: ' name]); end
                        end
                    elseif strcmp(name, info.Attributes(ii).Name)
                         try
                            data = h5readatt(info.Filename, root_location, name);
                             if iscell(data)
                                 data = data{1};
                             end      
                             found = 1;
                         catch 
                             if obj.debug_bit, disp(['failed to load attribute: ' name]); end
                         end
                    end
                    
                end
                
                if found
                    return;
                end
                
            end
                            
        end
        
        function groups = checkForSubGroup(obj, info, prop) % to be depricated!
            
            import util.text.*;
            
            if isfield(info, 'Groups')
               
                for ii = 1:length(info.Groups)

                    name = slash_append(info.Name, prop.name);

                    if obj.debug_bit>=10, disp(['comparing name= ' name ' with info.Groups(ii).Name= ' info.Groups(ii).Name]); end

                    for jj = 1:length(prop.alias)+1 % plus one for the primary name

                        if jj>1, name = slash_append(info.Name, prop.alias{jj-1}); end

                        if strcmp(name, info.Groups(ii).Name)

%                             index = strncmp({info.Groups.Name}, name, length(name)); % index of group names that begin with "name"
                            index = ~cellfun(@isempty, regexp({info.Groups.Name}, [name '(_\d)*$'])); % index of group names that are either name or name_# 
                            groups = {info.Groups(index).Name}; % get the relevant group names back to the calling object

                            return;

                        end

                    end

                end
                
            end
            
            groups = {};
                
        end
                
        function writeText(obj, class_obj, filename, if_exist, recursive) % write log file
            
            import util.text.cs;
            
            if obj.saving_lock
                return;
            end
            
%             disp(['this is writeText for ' obj.classname ' writing file: ' filename]);
            
            if nargin<4 || isempty(if_exist)
                if_exist = 'overwrite';
            end
            
            if nargin<5 || isempty(recursive)
                recursive = 1;
            end
            
            folder = fileparts(filename);
            
            if ~isempty(folder) && ~exist(folder, 'dir')
                mkdir(folder);
            end
            
            if exist(filename, 'file')
                
                if cs(if_exist, 'overwrite')
                    fid = fopen(filename,'wt+');
                elseif cs(if_exist, 'append')
                    fid = fopen(filename, 'at');
                else
                    error(['cannot overwrite or append to file: ' filename]);
                end
                
            else
                fid = fopen(filename,'wt+');
            end
            
            try % write the metadata for this object and close the file
                
                % header with classname
                fprintf(fid, '%s %s\n\n', obj.textheader, obj.classname);
                
                width = 5; % minimal width
                
                % find the required minimal width
                for ii = 1:length(obj.list)
                    
                    if obj.list(ii).transient
                        continue;
                    end
                    
                    if cs(obj.list(ii).type,'attribute') || cs(obj.list(ii).type,'data')
                        
                        if width<length(obj.list(ii).name)
                            width = length(obj.list(ii).name);
                        end
                        
                    end
                    
                end
                
                val_width = 12;
                precision = val_width-4;
                
                % write all attributes of the object
                for ii = 1:length(obj.list)
                    
                    try
                    
                        if obj.list(ii).transient
                            continue;
                        end
                        
                        if cs(obj.list(ii).type,'attribute') && ~isempty(class_obj.(obj.list(ii).name))
                            
                            name = obj.list(ii).name;
                            value = class_obj.(obj.list(ii).name);
                            
                            if isnumeric(value)
                                
                                fprintf(fid, '%*s: ', width, name);
                                
                                if any(value(:)>999999999) || any(isnan(value(:))) % if it is too large
                                    for jj = 1:size(value,1)
                                        if jj>1, fprintf(fid,'%*s  ', width,''); end
                                        for kk = 1:size(value,2), fprintf(fid, '% *.*e ', val_width, precision-3, value(jj,kk)); end
                                        fprintf(fid, '\n');
                                    end
                                elseif any(logical(mod(value(:),1))) % if it is floating point (under 999)
                                    for jj = 1:size(value,1)
                                        if jj>1, fprintf(fid,'%*s  ', width,''); end
                                        for kk = 1:size(value,2), fprintf(fid, '% *.*f ', val_width, precision, value(jj,kk)); end
                                        fprintf(fid, '\n');
                                    end
                                else % if integer (regardless of the container class)
                                    for jj = 1:size(value,1),
                                        if jj>1, fprintf(fid,'%*s  ', width,''); end
                                        for kk = 1:size(value,2), fprintf(fid, '% *d ', val_width, floor(value(jj, kk))); end
                                        fprintf(fid, '\n');
                                    end
                                    
                                end
                                
                                %                         fprintf(fid,'\n');
                                
                            elseif ischar(value)
                                
                                fprintf(fid, '%*s: %*s%s\n',width, name, val_width-1, '',value);
                                
                            elseif iscellstr(value)
                                
                            end
                            
                            
                        elseif cs(obj.list(ii).type, {'data', 'object'}) && ~builtin('isempty', class_obj.(obj.list(ii).name))
                            
                            name = obj.list(ii).name;
                            fprintf(fid, '%*s: [%dx%d', width, name, size(class_obj.(obj.list(ii).name),1), size(class_obj.(obj.list(ii).name),2));
                            for jj = 3:10
                                if size(class_obj.(obj.list(ii).name), jj)>1
                                    fprintf(fid, 'x%d',  size(class_obj.(obj.list(ii).name), jj));
                                end
                            end
                            
                            fprintf(fid, ' %s]\n', class(class_obj.(obj.list(ii).name)));
                            
                        end
                    
                    catch ME
                        disp(['Error saving ' obj.list(ii).name]);
                        warning(ME.getReport);
                    end
                    
                end
                
            catch ME
                fprintf(fid, '\n');
                fclose(fid);
                rethrow(ME);
            end
            
            fprintf(fid, '\n');
            fclose(fid);
            
            if recursive
                
                obj.saving_lock = 1; % if sub object has pointer back to this, saving lock will prevent a loop
                
                try
                    
                    for  ii = 1:length(obj.list)
                        
                        %                     disp(['---> ii= ' num2str(ii) ' name: ' obj.list(ii).name ' lock= ' num2str(obj.saving_lock)]);
                        
                        try
                            
                            if obj.list(ii).transient || obj.list(ii).dependent
                                continue;
                            end
                            
                            if cs(obj.list(ii).type, 'object')
                                
                                sub_object = class_obj.(obj.list(ii).name);
                                
                                if isa(sub_object, 'datetime')
                                    continue;
                                end
                                
%                                 disp(['name= ' obj.list(ii).name]);
                                
                                if ~isempty(sub_object)
                                    
                                    try
                                        meta_name = obj.getMetaName(sub_object(1));
                                        if isempty(meta_name)
                                            meta_name = obj.getMetaName(sub_object);
                                        end
                                    catch
                                        meta_name = obj.getMetaName(sub_object);
                                    end
                                    
                                    if isempty(meta_name)
                                        continue;
                                    end
                                    
                                    try
                                        
                                        for jj = 1:length(sub_object)
                                            
                                            if ~isempty(sub_object(jj).(meta_name))
                                                sub_object(jj).(meta_name).writeText(sub_object(jj), filename, 'append', recursive);
                                            end
                                        end
                                        
                                    catch
                                        meta_obj = sub_object.(meta_name);
                                        meta_obj.writeText(sub_object, filename, 'append', recursive);
                                    end
                                end
                            end
                            
                        catch ME
                            disp(['Error saving ' obj.list(ii).name]);
                            warning(ME.getReport);
                        end
                        
                        %                     disp(['<--- ii= ' num2str(ii) ' name: ' obj.list(ii).name ' lock= ' num2str(obj.saving_lock)]);
                        
                    end
                    
                catch ME
                    obj.saving_lock = 0;
                    rethrow(ME);
                end
                
                obj.saving_lock = 0;
                
%                 disp(['finished, lock= ' num2str(obj.saving_lock)]);
                                
            end
           
%             disp(['ending writeText for ' obj.classname]);
            
        end
        
        function success = readText(obj, class_obj, filename, recursive)
              
            import util.text.cs;
            
            if obj.saving_lock
                success = 1;
                return;
            end
            
            if ~exist(filename, 'file')
                error(['cannot find filename: ' filename]);
            end
           
            if nargin<4 || isempty(recursive)
                recursive = 0;
            end
            
%             if recursive
%                 warning('recursive option is not yet implemented!');
%             end
            
            fid = fopen(filename, 'r');
            success = 0;
            
            for ii = 1:10000 % find the header
                               
                tline = fgetl(fid);
                if ~ischar(tline), break; end
                if cs(regexprep([obj.textheader obj.classname], '[^\w'']',''), regexprep(tline,'[^\w'']',''))
                    success = 1;
                    break;
                end
                
            end
            
            if ~success
               for k = 1:length(obj.prev_classnames)
                   frewind(fid);
                   for ii = 1:10000 % find the header
                       
                       tline = fgetl(fid);
                       if ~ischar(tline), break; end
                       if cs(regexprep([obj.textheader obj.prev_classnames{k}], '[^\w'']',''), regexprep(tline,'[^\w'']',''))
                           success = 1;
                           break;
                       end                       
                   end
                   if success, break; end
               end
            end
                                    
            for ii = 1:10000 % read the data one by one
                               
                tline = fgetl(fid);
                if ~ischar(tline), break; end
                if cs(obj.textheader, tline), break; end
                
                c = strsplit(tline, ':'); 
                if isempty(c) 
                    break; 
                elseif length(c)==1
                    val = [];
                else                   
                    val = strtrim(c{2});                
                end
                
                name = strtrim(c{1});
                
                if ~isempty(name) 
                    
                    p = obj.getProperty(name);
                    
                    if ~isempty(p) && ~p.transient && cs(p.type, 'attribute')
                        try
                            name = p.name;
                            num = str2num(val);
                            if isempty(num)
                                class_obj.(name) = val;
                            else
                                class_obj.(name) = num;
                            end
                        catch ME
                            if obj.debug_bit, disp(['cannot set ' name]); end
                        end
                    end
                end
                      
            end

            fclose(fid);
            
            if recursive

                obj.saving_lock = 1;

                try 

                    for ii = 1:length(obj.list)
                        
%                         disp(['class: ' obj.classname ' | member: ' obj.list(ii).name]);

                        if ~obj.list(ii).transient && ~obj.list(ii).dependent && ~isempty(class_obj.(obj.list(ii).name)) && cs(obj.list(ii).type, 'object') % dependent OBJECTS are not saved!

                            name = obj.list(ii).name;

                            metaname = obj.getMetaName(class_obj.(name));

                            if ~isempty(metaname)                            
                                success = class_obj.(name).(metaname).readText(class_obj.(name), filename, recursive);
                            end

                        end

                    end

                catch ME
                    obj.saving_lock = 0;
                    rethrow(ME);
                end

                obj.saving_lock = 0; 

            end
            
        end
        
        function s = makeAttributeStruct(obj, class_obj)
           
           counter = 0;
           for jj = 1:length(obj.list)
               if ~obj.list(jj).transient && cs(obj.list(jj).type, 'attribute')
                   counter = counter + 1;
               end
           end
           
           arguments{2*counter} = [];
           
           counter = 1;
           for jj = 1:length(obj.list)
               if ~obj.list(jj).transient && cs(obj.list(jj).type, 'attribute')
                   arguments{counter*2-1} = obj.list(jj).name;
                   values{length(class_obj)} = [];
                   for ii = 1:length(class_obj)
                       values{ii} = class_obj(ii).(obj.list(jj).name);
                   end
                   arguments{counter*2} = values;
                   counter = counter + 1;
               end
           end
           
           s = struct(arguments{:});
           
        end
        
        function unlock(obj, class_obj)
                       
            import util.text.cs;
            
            obj.saving_lock = 0;
            
            if nargin>2 && ~isempty(class_obj)
                
                for ii = 1:length(obj.list)
                
                    if cs(obj.list(ii).type, 'object')
                    
                        sub_object = class_obj.(obj.list(ii).name);
                        
                        if ~isempty(sub_object)
                            
                            try
                                meta_name = obj.getMetaName(sub_object(1));
                                if isempty(meta_name)
                                    meta_name = obj.getMetaName(sub_object);
                                end
                            catch
                                meta_name = obj.getMetaName(sub_object);
                            end
                            
                            if isempty(meta_name)
                                continue;
                            end
                            
                            sub_object.(meta_name).unlock(1);
                            
                        end
                            
                    end
                        
                end
                    
            end
            
        end
        
    end
    
    methods % reset methods
        
        function full_reset(obj, class_obj)
            
        end
        
        function att_reset(obj,class_obj)
        
        end
            
        function data_reset(obj, class_obj)
            
        end
        
        function object_reset(obj, class_obj)
        
        end
            
    end
    
    methods % printouts
       
        function printout(obj)
           
            for ii=1:length(obj.list), 
                
                fprintf('%-25s %10s',obj.list(ii).name, obj.list(ii).type); 
                
                if obj.list(ii).transient, fprintf(' * '); end 
                
                for jj = 1:length(obj.list(ii).alias)
                   
                    fprintf(' "%s" ', obj.list(ii).alias{jj});
                    
                end
                
                fprintf('\n'); 
            
            end

        end
        
    end
    
    methods (Static=true)
       
        function out_str = addSlash(str)
           
            if isempty(str)
                out_str = '/';
                return;
            end
                
            if str(end)=='/'
                out_str = str;
            else
                out_str = [str '/'];
            end
            
        end
        
        function out_str = remSlash(str)
                       
            if isempty(str)
                out_str = '';
                return;
            end
                
            if str(end)~='/'
                out_str = str;
            else
                out_str = str(1:end-1);
            end
            
        end
           
        function meta_name = getMetaName(class_obj)
           
            meta_name = '';
            
            if isempty(class_obj), return; end
            if length(class_obj)>1
                if iscell(class_obj)
                    class_obj = class_obj{1};
                else
                    class_obj = class_obj(1);
                end
            end
            
            mc = metaclass(class_obj);
            pl = mc.PropertyList;
            
            for ii = 1:length(pl)
               
                if isa(class_obj.(pl(ii).Name), 'util.MetaData')
                    meta_name = pl(ii).Name;
                    break;
                end
                
            end
                        
        end
        
        function full_copy(class_obj, other_obj)
            
            meta_name = util.MetaData.getMetaName(class_obj);
            
            class_obj.(meta_name) = util.MetaData(other_obj.(meta_name));
            obj = class_obj.(meta_name);
            
            for ii = 1:length(obj.list)
               
                if ~obj.list(ii).copyable || obj.list(ii).dependent
                    continue;
                end
                
                name = obj.list(ii).name;
                type = obj.list(ii).type;
                if obj.list(ii).transient
                    continue;
                end
                                
                if strcmp(type, 'object') && builtin('isempty', other_obj.(name)) && ~obj.list(ii).external % if it is an object and it is empty...
                    sub_class_name = class(class_obj.(name));
                    class_obj.(name) = eval([sub_class_name '.empty;']);
                elseif strcmp(type, 'object') && ~isempty(other_obj.(name)) && ~obj.list(ii).external
                    if isobject(class_obj.(name))
                        sub_class_name = class(class_obj.(name));
                    elseif isobject(other_obj.(name))
                        sub_class_name = class(other_obj.(name));
                    else 
                        warning(['Cannot identify class of "' name '". Skipping to next item...']);
                        continue;
                    end
                    
                    if isa(other_obj.(name), 'handle')
                    
                        if length(other_obj.(name))>1
                            
                            for jj = 1:length(other_obj.(name))
                                str = sprintf('%s(other_obj.%s(%d))\n', sub_class_name, name,jj);
                                class_obj.(name)(jj) = eval(str); % use that class' copy-constructor!
                            end
                        
                        else
                            
                            str = sprintf('%s(other_obj.%s)\n', sub_class_name, name);
                            class_obj.(name) = eval(str); % use that class' copy-constructor!
                            
                        end
%                     str = sprintf('%s(other_obj.%s)\n', sub_class_name, name);
%                     class_obj.(name) = eval(str); % use that class' copy-constructor!
                    else
                        class_obj.(name) = other_obj.(name);
                    end

                elseif strcmp(type, 'object') && obj.list(ii).external % external (pointer-type, shared resource object)
                    class_obj.(name) = other_obj.(name);
                elseif ~strcmp(type, 'object')  % non-objects 
                    class_obj.(name) = other_obj.(name);
                else
                    disp(['Warning: MetaData cannot figure out what to do with member ' name ' in class ' class(class_obj)]);
                end
                
            end
            
            
        end
        
        function idx = array_index(str, base_str)
            
            idx = [];
            if length(str)<length(base_str)
                return;
            end
            
            
            %  make sure the entire base_str is contained in str
            for ii = 1:length(base_str)
                if base_str(ii)~=str(ii), return; end
            end
            
            if length(str)==length(base_str)
                idx = 0;
                return;
            end
            
            if str(length(base_str)+1)=='(' && str(end)==')' && util.MetaData.isdigits(str(length(base_str)+2:end-1))
                idx = sscanf(str(length(base_str)+2:end-1), '%d');
                if idx==0
                    idx = [];
                    return; 
                end
            end            
            
        end
        
        function idx = cell_index(str, base_str)
            
            idx = [];
            if length(str)<length(base_str)
                return;
            end
            
            
            %  make sure the entire base_str is contained in str
            for ii = 1:length(base_str)
                if base_str(ii)~=str(ii), return; end
            end
            
            if length(str)==length(base_str)
                idx = 0;
                return;
            end
            
            if str(length(base_str)+1)=='{' && str(end)=='}' && util.MetaData.isdigits(str(length(base_str)+2:end-1))
                idx = sscanf(str(length(base_str)+2:end-1), '%d');
                if idx==0
                    idx = [];
                    return; 
                end
            end            
            
        end
        
        function check = isdigits(str)
            
            if ~isempty(str) && all(str>='0' & str<='9')
                check = 1;
            else
                check = 0;
            end
            
        end
        
    end
        
end