function S = sum2(I, outtype)

    if nargin<2 || isempty(outtype)
        outtype = 'double';
    end
        
    S = sum(sum(I,1, outtype),2, outtype);

end