function plot_ev_fit(values, varargin)

    import util.text.*;

    if nargin<1
        fprintf('plot_ev_fit(values, varargin)\n axes: choose the axes to draw to.\n legend: what to state on the legend (default= "data").\n');
    end
    
    ax = [];
    leg_str = 'data';

    for ii = 1:2:length(varargin)
        
        key = varargin{ii};
        val = varargin{ii+1};
        
        if cs(key, {'axes', 'axis'})
            ax = val;
        elseif cs(key, {'legend', 'leg_str'})
            leg_str = val;
        end
        
    end

    if isempty(ax)
        ax = gca;
    end
    
    phat = evfit(-values);
    
    h = histogram(ax, values, 'BinMethod', 'sqrt', 'normalization', 'pdf');
    
    hold on;
    
    plot(h.BinEdges+h.BinWidth/2, evpdf(-h.BinEdges+h.BinWidth/2, phat(1), phat(2)));
    
    hold off;
        
    legend({leg_str, ['fit: \mu= ' f2s(-phat(1)) ' | \sigma= ' f2s(phat(2))]});

end