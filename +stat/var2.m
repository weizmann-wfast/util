function V = var2(I)

    M = util.stat.mean2(I); % could have 3rd or 4th non scalar dimensions... 

    V = util.stat.mean2(bsxfun(@minus, I, M).^2);
   
end