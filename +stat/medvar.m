function V = medvar(values, dim, varargin)
% usage: V = medvar(values, dim, varargin)
% calculates the median of the square deviations from the mean for "values"
% along the dimension "dim" (default=1). 

    v2 = (values-mean(values)).^2;
    V = median(v2);
    
end