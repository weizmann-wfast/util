function [M, m_ind] = center_max(I, pixels)

    if nargin<2 || isempty(pixels)
        pixels = 5;
    end
       
    ind(1) = size(I,1)/2-1;
    ind(2) = size(I,2)/2-1;

    y_min = ind(1) - floor(pixels/2);
    y_max = ind(1) + ceil(pixels/2);
    
    x_min = ind(2) - floor(pixels/2);
    x_max = ind(2) + ceil(pixels/2);    
    
    I_clipped = I(y_min:y_max, x_min:x_max,:);
    
    N = size(I_clipped,3);
    
    M = zeros(N,1);
    m_ind = zeros(N,2);
    
    for ii = 1:N
        [M(ii), m_ind(ii,:)] = util.stat.max2(I_clipped(:,:,ii));
    end

end