function S = corner_var(img, num_pix)

    img = double(img);

    if nargin<2 || isempty(num_pix)
        num_pix = 0.15;
    end
    
    if num_pix>0 && num_pix<1 
        num_pix = ceil(min(size(img, 1), size(img,2))*num_pix);
    end

    if nargin<2 || isempty(num_pix)
        num_pix=20;
    end

    S = mean(var(img(1:num_pix,1:num_pix,:,:))) + ...
        mean(var(img(1:num_pix,end-num_pix:end,:,:))) + ...
        mean(var(img(end-num_pix:end,1:num_pix,:,:))) + ...
        mean(var(img(end-num_pix:end,end-num_pix:end,:,:)));

    S = S/4;

end