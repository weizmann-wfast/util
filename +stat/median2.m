function M = median2(I)
% calculates median of images. Accepts 2D, 3D and 4D matrices... 

    M = zeros(1,1,size(I,3), size(I,4));

    for ii = 1:size(I,3)
        for jj = 1:size(I,4)
            single_image = I(:,:,ii,jj);
            M(1,1,ii,jj) = median(single_image(:));
        end
    end

end