function [w, V] = im_fwhm(image)

    import util.stat.corner_mean;
    import util.stat.max2;
    import util.vec.tocolumn;

    image = double(image) - corner_mean(image, 20);

    [m, mid] = max2(image);
    
    factor = 5;
    
    v1 = tocolumn(resample(image(:,mid(2),:), factor,1));
    v2 = tocolumn(resample(image(mid(1),:,:), factor,1));

    L = min([length(v1) length(v2)]);
    
    v1 = v1(1:L);
    v2 = v2(1:L);
    
    V = [v1 v2];
    
    w = mean([sum(v1>=0.5*m) sum(v2>=0.5*m)])/factor;
    
end