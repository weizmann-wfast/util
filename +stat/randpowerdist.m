function r = randpowerdist(out_size, index, lower_limit, upper_limit)
% generates some numbers from a power law distribution. Usage: randpowerdist(size, index, lower, upper)   
    
    if nargin<1 || isempty(out_size)
        out_size = 1;
    end
    
    if nargin<2 || isempty(index)
        index = 1;
    end
    
    if nargin<3 || isempty(lower_limit)
        lower_limit = 0;
    end
    
    if nargin<4 || isempty(upper_limit)
        upper_limit = 1;
    end
    
    if lower_limit>upper_limit
        error(['lower limit ' num2str(lower_limit) ' must be smaller than upper limit ' num2str(upper_limit)]);
    end
    
    n = rand(out_size);
    
%     r = lower_limit + (upper_limit-lower_limit)*n.^(1/(-index+1));
% stolen shamelessly from https://www.mathworks.com/matlabcentral/newsreader/view_thread/301276
    
    r = (lower_limit.^(index+1) + (upper_limit.^(index+1)-lower_limit.^(index+1)).*n ).^(1/(index+1));
% stolen shamelessly from https://stackoverflow.com/questions/918736/random-number-generator-that-produces-a-power-law-distribution
    
end