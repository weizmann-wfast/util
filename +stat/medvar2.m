function V = medvar2(M, varargin)
% calculates medvar of images. Accepts 2D, 3D and 4D matrices... 

    V = zeros(1,1,size(M,3), size(M,4));

    for ii = 1:size(M,3)
        for jj = 1:size(M,4)
            single_image = M(:,:,ii,jj);
            V(1,1,ii,jj) = util.stat.medvar(single_image(:));
        end
    end

end