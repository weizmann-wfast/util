function M = corner_mean(img, num_pix)

    import util.stat.*;

    if nargin<2 || isempty(num_pix)
        num_pix = 0.15;
    end
    
    if num_pix>0 && num_pix<1 
        num_pix = ceil(min(size(img, 1), size(img,2))*num_pix);
    end

    if num_pix<=0
        M = 0;
        return;
    end

    img = double(img);

    if nargin<2 || isempty(num_pix)
        num_pix=20;
    end

    M = mean2(img(1:num_pix,1:num_pix,:,:)) + ...
        mean2(img(1:num_pix,end-num_pix:end,:,:)) + ...
        mean2(img(end-num_pix:end,1:num_pix,:,:)) + ...
        mean2(img(end-num_pix:end,end-num_pix:end,:,:));

    M = M/4;

end