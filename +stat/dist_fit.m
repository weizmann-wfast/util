function fit_results = dist_fit(data, varargin)
% usage: fit_results = dist_fit(data, varargin) fits data to distribution


% need to change the core of this to matlab's native mle and mlecov...

    dist_type = 'gaussian';
    nbins = sqrt(length(data)); % default number of bins... 
    plotting = 0;
    ax = [];
    font_size = 18;
    
    if ~isempty(varargin) && mod(length(varargin),2)==1
        varargin{end+1} = 1; % positive approach
    end

    for ii = 1:2:length(varargin)
        
        if check_str(varargin{ii}, {'distrubution', 'dist_type'})
            dist_type = varargin{ii+1};
        elseif check_str(varargin{ii}, {'nbins', 'numbins'})
            nbins = varargin{ii+1};
        elseif check_str(varargin{ii}, 'plotting')
            plotting = parse_bool(varargin{ii+1});
        elseif check_str(varargin{ii}, {'axis','axes'})
            ax = varargin{ii+1};
        elseif check_str(varargin{ii}, {'fontsize', 'font_size'})
            font_size = varargin{ii+1};
        end
        
    end
    
    nbins = round(nbins);
    
%     [counts, bins] = hist(data, nbins); % just to get estimates... 
    
    if check_str(dist_type, 'gaussian')
        probability = @(x,offset,width) 1./sqrt(2*pi*width^2)*exp(-0.5*(x-offset).^2./width.^2);
        b_initial = [median(data), std(data)]; % initial values for parameters... 
        LL = @(b) sum(-2*log(probability(data, b(1), b(2)))); % log likelihood
        coefficients = {'offset', 'width'};
        model_str = 'gaussian';
    end
    
    b_found = fminsearch(LL, b_initial);
        
    fit_results = struct();
    
    for ii = 1:length(b_found)
        if ii>length(coefficients) || isempty(coefficients{ii})
            coefficients{ii} = double('a')+ii;
        end
        
        fit_results.(coefficients{ii}) = b_found(ii);
        
    end
    
    if plotting
        if isempty(ax)
            ax = gca;
        end
        
        [counts, bins] = hist(data, nbins);
        
        N = sum(counts(1:end-1).*(bins(2:end)-bins(1:end-1)));
        
        bar(ax, bins, counts/N);
        
        hold on; 
        
        str = 'h = plot(bins, probability(bins';
        for ii = 1:length(b_found)
            str = [str ', b_found(' num2str(ii) ')'];
        end
        
        str = [str '));'];
        eval(str);
        h.Color = 'red';
        hold off;
        
        legend({'data', 'fit'});
        
        model_text = ['model: ' model_str];
        for ii = 1:length(b_found)
            model_text = [model_text char(10) coefficients{ii} '= ' f2s(b_found(ii))];
        end
        
        text(0.1, 0.9, model_text, 'Color', 'Red', 'FontSize', font_size, 'Units', 'Normalized');
        inner_title(['nbins= ' num2str(nbins)], 'Position', 'Bottom', 'Color', 'Red', 'FontSize', font_size);
        inner_title(['likelihood= ' f2s(exp(-0.5*LL(b_found)))], 'Position', 'Left', 'Color', 'Red', 'FontSize', font_size);
        inner_title(['N samples= ' num2str(length(data))], 'Position', 'Right', 'Color', 'Red', 'FontSize', font_size);
        drawnow;
        
    end
    

end