function S = corner_std(img, num_pix)
    
    if nargin<2
        num_pix = [];
    end
    
    S = sqrt(util.stat.corner_var(img, num_pix));

end