function fr = line_fit(x,y,varargin)

    import util.text.cs
    import util.text.parse_bool;
    import util.vec.tocolumn;
    
    x = tocolumn(x);
    y = tocolumn(y);
    
    type = [];
    add_const = 0;
    
    for ii = 1:2:length(varargin)
        if cs(varargin{ii}, 'type')
            type = varargin{ii+1};
        elseif cs(varargin{ii}, {'add_const', 'const'})
            add_const = parse_bool(varargin{ii+1});
        end
    end

    if isempty(type)
        type = 'gaussian';
    end
        
    if cs(type, 'gaussian')
        
        amp = max(abs(y));
        mu = sum(abs(y).*x)./sum(abs(y));
%         sig = sum(abs(y).*x.^2)./sum(abs(y)) - mu.^2;
        sig = (sum(abs(y)>0.5*amp))./2.355;

        if add_const==0
 
%             disp(['amp= ' num2str(amp) ' | mu= ' num2str(mu) ' | sig= ' num2str(sig)]);

            fo=fitoptions('Method', 'NonlinearLeastSquares', 'StartPoint', [amp mu sig]);

            ft = fittype('a*exp(-0.5*(x-b)^2/c^2)', 'options', fo);

        else
            
            const = median(y);
            
            disp(['amp= ' num2str(amp) ' | mu= ' num2str(mu) ' | sig= ' num2str(sig) ' | const= ' num2str(const)]);

            fo=fitoptions('Method', 'NonlinearLeastSquares', 'StartPoint', [amp mu sig const]);

            ft = fittype('a*exp(-0.5*(x-b)^2/c^2)+d', 'options', fo);

        end

        fr = fit(x,y,ft);
        
    end
    
end