function S = nansum2(I)

    S = nansum(nansum(I,1),2);

end