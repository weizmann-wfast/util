function w = fwhm(I, varargin)

    import util.text.cs;
    import util.stat.max2;
    import util.stat.sum2;
        
    % defaults
    method = 'surf';
    subtraction = 'median';
    oversample = [];
    directions = [];
    vertex = [];
    peak_interp = [];
    
    try % parse varargin

        if ~isempty(varargin) && mod(length(varargin),2)==1
                varargin{end+1} = 1; % positive approach
        end
        
        for ii = 1:2:length(varargin)
            if cs(varargin{ii}, 'method')
                method = varargin{ii+1};
            elseif cs(varargin{ii}, 'subtraction')
                subtraction = varargin{ii+1};
            elseif cs(varargin{ii}, 'oversample')
                oversample = varargin{ii+1};
            elseif cs(varargin{ii}, 'directions')
                directions = varargin{ii+1};
            elseif cs(varargin{ii}, 'vertex')
                vertex = varargin{ii+1};
            elseif cs(varargin{ii}, 'peak_interp')
                peak_interp = varargin{ii+1};
            end            
        end

        if isempty(directions)
            directions = [0 90];
        end

        if isempty(vertex)
            vertex = 'max';
        end

    catch ME
        rethrow(ME);
    end
    
    if ischar(vertex)
        if cs(vertex, 'max')
            [~, vertex] = max2(I);
        elseif cs(vertex, 'center')
            vertex = ceil((size(I)+1)./2);
        end
    end
    
    if cs(peak_interp, 'fill')
        mask = false(size(I));
        mask(vertex(1), vertex(2)) = 1;
        I = regionfill(I, mask);
    end
    
    if cs(subtraction, 'median')
        I = I-median(I(:));
    elseif cs(subtraction, 'mean')
        I = I-mean(I);
    end
    
    if oversample
        I = imresize(I, oversample);
    end
    
    if cs(method, 'surf')
        mx = max2(I);
        S = sum2(I>0.5*mx);
    end
    
    w = sqrt(S/pi)*2;

    if oversample
        w = w./oversample;
    end
        
end