function c = tocell(input)

    if iscell(input)
        c = input;
    else
        c = {input};
    end
    
end