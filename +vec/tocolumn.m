function v_col = tocolumn(vector)

    if isempty(vector)
        v_col = vector;
        return;
    end
    
    vector = squeeze(vector);

    if ~isvector(vector)
        disp(['warning: input to "tocolumn" is a matrix of size ' num2str(size(vector))]);
        v_col = vector;
        return;
    end
    
    if isrow(vector)
        vector = vector';
    end
    
    v_col = vector;

end