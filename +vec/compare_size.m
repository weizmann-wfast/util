function output = compare_size(M1, M2, dims, scalars_allowed)
   
    if nargin<2
        error('must input 2 matrices to "compare_size"');
    end
        
    if isempty(M1) && isempty(M2) 
        error('both inputs to "compare_size" are empty!');
    elseif isempty(M1)
        error('input 1 to "compare_size" is empty!');
    elseif isempty(M2)
        error('input 2 to "compare_size" is empty!');
    end
    
    if nargin<3 || isempty(dims)
        a = ndims(M1);
        b = ndims(M2);
        c = max(a,b);
        dims = 1:c;
    end
    
    if nargin<4 || isempty(scalars_allowed)
        scalars_allowed = 0;
    end
    
    success = 1; % positive approach
    
    if scalars_allowed
        
        for ii = 1:length(dims)
            
            if size(M1, dims(ii))>1 && size(M2, dims(ii))>1 && size(M1, dims(ii))~=size(M2, dims(ii))
                success = 0;
                break;
            end
            
        end
        
    else
        
        for ii = 1:length(dims)
            
            if size(M1, dims(ii))~=size(M2, dims(ii))
                success = 0;
                break;
            end
            
        end
        
    end
    
    if nargout>0
        output = success;
    else
        if success==0
            error(['size(' inputname(1) ')= ' num2str(size(M1)) ' | size(' inputname(2) ')= ' num2str(size(M2))]);
        end
    end
    
end

