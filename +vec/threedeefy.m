function [M_out, secondary_out] = threedeefy(M_in, func)
% applies func (that can deal with 3D matrices and returns same size output) 
% to M_in even if M_in is 4D (or more), it linearizes the higher dimensions then 
% reshapes the output back to the same shape as M_in.

    if nargin<2 || isempty(func) || ~isa(func, 'function_handle')
        error('must input a function handle as second argument!');
    end

    if ndims(M_in)<4
        M_out = func(M_in);
        return;
    end

    S = size(M_in);
    
    
    if nargout<2
        M_out = func(M_in(:,:,:)); % linearize any dimensions higher than 3
    else
        [M_out, secondary_out] = func(M_in(:,:,:)); % linearize any dimensions higher than 3
        secondary_out = reshape(secondary_out, [size(secondary_out,1), size(secondary_out,2), S(3:end)]);
    end
    
    S_new = size(M_out);
    M_out = reshape(M_out, [S_new(1), S_new(2), S(3:end)]);

    
end