function v_row = torow(vector)

    if isempty(vector)
        v_row = vector;
        return;
    end

    vector = squeeze(vector);
    
    if ~isvector(vector)
        disp(['warning: input to "torow" is a matrix of size ' num2str(size(vector))]);
        v_row = vector;
        return;
    end
    
    if iscolumn(vector)
        vector = vector';
    end
    
    v_row = vector;

end