function val = subind(V, index)

    if nargin<2 || isempty(index)
        index = 1;
    end

    val = V(index);

end