function v_row = topages(vector)

    if isempty(vector)
        v_row = vector;
        return;
    end
    
    vector = squeeze(vector);

    if ~isvector(vector)
        disp(['warning: input to "topages" is a matrix of size ' num2str(size(vector))]);
        v_row = vector;
        return;
    end
    
    if isrow(vector)
        vector = vector';
    end
    
    v_row = permute(vector, [3,2,1]);

end