function y = ceilfix(x)

    y = ceil(abs(x)).*sign(x);

end