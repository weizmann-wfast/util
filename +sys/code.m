function out_str = data

    str = getenv('MAT');
    if isempty(str)
        error('no environemntal variable "MAT" found in system...');
    end

    if nargout==0
        cd(str);
    else
        out_str = str;
    end

end