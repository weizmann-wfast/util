function out = wait_print(future, timeout, grain)

    if nargin<2 || isempty(timeout)
        timeout = 60; % seconds!
    end
    
    if nargin<3 || isempty(grain)
        grain = 1; % seconds!
    end
    
    fprintf('Waiting on %s', func2str(future.Function));

    for ii = 1:1e9

        if ~check_str(future.State, {'running','queued'}) || ii*grain>timeout
            break;
        end
        
        fprintf('.');
        
        pause(grain);
        
    end
    
    fprintf('done!\n');
        
    out = future.fetchOutputs;
    
end