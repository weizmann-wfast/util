function out_str = data

    str = getenv('DATA');
    if isempty(str)
        error('no environemntal variable "DATA" found in system...');
    end

    if nargout==0
        cd(str);
    else
        out_str = str;
    end

end