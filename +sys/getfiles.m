function [files] = getfiles(expression, recursive, type, directory, excludeExpression)
%GETFILES Returns files matching the desired glob or regular expression
%   Inputs:
%      expression: expresion (regular or glob)
%      recursive (optional): 1 = recursive (default), 0 = base directory only
%      type (optional): 'glob' (default) or 'regexp'
%           This function will try to determine the type.
%      directory (optional): Specify the directory to search in. If this is
%           omitted, the current directory will be used.
%      excludeExpression (optional): Removes any files that match
%           expression, but that also contain the excludeExpression
%   Outputs:
%      files = cell array of files with names matching the regular
%      expression
% 
%      Author: Brian P. Battaglia


    % Set inputs if they are not defined
    if nargin < 2
        recursive = 1;
    end

    if nargin < 3
        type = 'glob';
    end

    if nargin >= 2
        if ~isnumeric(recursive) && ~islogical(recursive)
            error('Recursive should be true of false')
        end
    end

    % If type is glob and symbols only found in regular expressions are used,
    % convert type to regexp and alert the user if glob was specified.

    if strcmpi(type,'glob')
        if ~isempty(regexpi(expression,'\\')) || ...
           ~isempty(regexpi(expression,'\?')) || ...
           ~isempty(regexpi(expression,'>')) || ...
           ~isempty(regexpi(expression,'<')) || ...
           isempty(regexpi(expression,'*')) || ...
           nargin >= 5
                type = 'regexp';
                if nargin == 3
                    warning('Regular expression matching will be used');
                end
        end
    end

    if ~strcmpi(type,'glob') && ~strcmpi(type,'regex') && ~strcmpi(type,'regexp')
        error('Unknown type')
    end

    if nargin < 4
        directory = pwd;
    end

    % Get paths depending on whether recursive search was selected
    if recursive == 0
        paths = directory;
    else
        paths = genpath(directory);
    end

    % Split paths from genpath into cell array
    paths = regexpi(paths,pathsep,'split');
    if isempty(paths{end})
        paths = paths(1:end-1);
    end

    if strcmpi(type,'glob')
        expression = regexprep(expression,'*','.*');
        if nargin >= 5
            excludeExpression = regexprep(excludeExpression,'*','.*');
        end
    end

    % Parse exclude regular expression and convert to cell array
    if nargin >= 5
        if ischar(excludeExpression)
            excludeExpression = regexpi(excludeExpression,pathsep,'split');
        end
        if isempty(excludeExpression{end})
            excludeExpression = excludeExpression(1:end-1);
        end
    end
    
    files = {};

    % Loop through paths
    for n = 1:length(paths)

        % Loop through files within paths and append files that match
        % expression to "files" array

        theseFiles = javadir(fullfile(paths{n}));
        matches = regexpi(theseFiles,expression);

        for k = 1:length(matches)
            if ~isempty(matches{k})
                fullName = fullfile(paths{n},theseFiles{k});
                if nargin >= 5
                    excludeMe = false;
                    for e = 1:length(excludeExpression)
                        if isdir(fullName) || ~isempty(regexpi(fullName,excludeExpression{e}))
                            excludeMe = true;
                        end
                    end
                    if excludeMe == false
                        files{end+1,1} = fullName; %#ok<AGROW>
                    end
                else
                    if ~isdir(fullName)
                        files{end+1,1} = fullName; %#ok<AGROW>
                    end
                end
            end
        end

    end

end

function files = javadir(directory)

    listing = java.io.File(directory);
    files = cell(listing.list);
    
end