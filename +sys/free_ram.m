function RAM = free_ram()

    if ispc
        
        M = memory;
        RAM = M.MaxPossibleArrayBytes;
    end
    
    if isunix
        comm=' free | grep Mem | awk ''{print $4}'' ';
        [~, RAM] = unix(comm);
        RAM = str2num(RAM)*1024;
        
    end

end