function I = lorentzian2(CX, CY, rot_frac, S)

    if nargin<2 || isempty(CY)
        CY = CX;
    end

    if isempty(CX) && ~isempty(CY)
        CX = CY;
    end
    
    if nargin<3 || isempty(rot_frac)
        rot_frac = 0;
    end
    
    if nargin<4 || isempty(S)
        S = ceil(max(CX, CY)*10);
    end

%     [x,y] = meshgrid(-ceil(S/2):floor(S/2)-1);
%     [x,y] = meshgrid(-floor((S+1)/2):floor((S-1)/2));
    [x,y] = meshgrid(-floor((S)/2):floor((S-1)/2));
    
    x2 = +x*cos(pi/2*rot_frac)+y*sin(pi/2*rot_frac);
    y2 = -x*sin(pi/2*rot_frac)+y*cos(pi/2*rot_frac);
    
    
    I = 1./((x2./CX).^2+(y2./CY).^2+1);
    
end