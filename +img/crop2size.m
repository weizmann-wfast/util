function M_out = crop2size(M_in, S_needed)
% crops the given array M_in to size M_size, leaving it in the middle. Won't
% grow the array...

    if isscalar(S_needed)
        S_needed = [1 1]*S_needed;
    else
        S_needed = S_needed(1:2);
    end
    
    S_in = size(M_in);
    S_in = S_in(1:2);

    if S_needed(1)<size(M_in,1) || S_needed(2)<size(M_in,2)
        
        gap = (S_in - S_needed)/2;
                
        y1 = 1+ceil(gap(1));
        y2 = S_in(1)-floor(gap(1));
        x1 = 1+ceil(gap(2));
        x2 = S_in(2)-floor(gap(2));
        
        M_out = M_in(y1:y2,x1:x2,:);
        
    else
        M_out = M_in;
    end
    
end