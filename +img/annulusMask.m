function M = annulusMask(ImSize, varargin)

    import util.text.cs;
    import util.img.gaussian2;
    import util.stat.sum2;
    
    R_min = 0;
    R_max = floor(min(ImSize)/2)-1; % what should be the default? -1 from image radius? -2? -3? 
    feather = [];
    
    for ii = 1:2:length(varargin)
        
        if cs(varargin{ii}, {'minimum_radius', 'r_min'}, 3)
            R_min = varargin{ii+1};
        elseif cs(varargin{ii}, {'maximum_radius', 'r_max'}, 3)
            R_max = varargin{ii+1};
        elseif cs(varargin{ii}, 'feather')
            feather = varargin{ii+1};
        end
        
    end
    
    if isscalar(ImSize)
        ImSize = [ImSize ImSize];
    end
    
    if feather
        k = gaussian2(feather,[],[],feather*2+1);
        k = k./(sum2(k));
        ImSize = ImSize+size(k)-1;
    end
    
    if ndims(ImSize)==3
        numPages = ImSize(3);
    else 
        numPages = 1;
    end
    
    ImSize = ImSize(1:2);
    
    [x,y] = meshgrid(1:ImSize(2), 1:ImSize(1));

    center = (ImSize/2)+0.5;
    
    x = x - center(2);
    y = y - center(1);
    
    dist = sqrt(x.^2+y.^2);

    M = R_max-dist;
    M(M>1) = 1;
    M(M<0) = 0;
    
    if R_min>0
        M2 = dist-R_min;
        M2(M2>1) = 1;
        M2(M2<0) = 0;        
        M = M & M2;
    end
    
    
%     M = M+M2;
%     
%     M = ones(ImSize);
%     mask1 = logical(logical((dist<R_min))+logical((dist>R_max)));
%     mask2 = logical( (dist-R_min>0).*(dist-R_min<1) ).*(dist-R_min+0.5)+logical( (dist-R_max>0).*(dist-R_max<1)).*(dist-R_max+0.5)
%     M(mask1) = 0;
%     M = M+mask2;
    
    if feather
        M = filter2(k, M, 'valid');
    end

end