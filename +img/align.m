function [M_shifted, C] = align(M_ref, M_new, varargin)
% aligns second matrix to position of the first: M_shifted = align(M_ref, M_new)
% uses conv_f or circular convolution to find the best correlation value,
% then uses imshift to move M_new to the right position relative to N_ref.

    import util.fft.*;
    import util.stat.*;
    import util.img.*;
    import util.text.*;
    
    data_type = 'double'; % can also choose "uint8" for instance
    padding = 'circ'; % can also choose "zeros"
    use_conv = 1;
    use_minimizer = 0;
    
    if ~isempty(varargin) && mod(length(varargin),2)==1
        varargin{end+1} = 1; % positive approach
    end
    
    for ii = 1:2:length(varargin)
    
        key = varargin{ii};
        val = varargin{ii+1};
        
        if cs(key, {'data_type', 'class', 'type'})
            data_type = val;
        elseif cs(key, 'padding')
            padding = val;
        elseif cs(key, {'use_conv', 'conv'}, 5)
            use_conv = val;
        elseif cs(key, {'use_minimizer', 'minimizer'}, 5)
            use_minimizer = val;
        end
        
    end
    
    if use_conv==0 && use_minimizer==0
        error('cannot align without using convolution or minimizer');
    end
    
    if cs(data_type, 'double')    
        M_ref = double(M_ref);
        M_new = double(M_new);
    elseif cs(data_type, 'uint8', 5)
        M_ref = uint8(M_ref*16/max2(M_ref));
        M_new = uint8(M_new*16/max2(M_new));
    else
        error(['unknown data_type option "' data_type '". Use double or uint8']);
    end
    
    idx = [0 0];
    
    if use_conv

        if cs(padding, 'zeros')
            C = conv_f(M_ref, M_new, 'crop', 'same', 'conj', 1);
        elseif cs(padding, 'circ')
            M_ref = pad2size(M_ref, size(M_new));
            M_new = pad2size(M_new, size(M_ref));
            C = fftshift2(real(ifft2(conj(fft2(M_ref)).*fft2(M_new))));
        else
            error(['unknown padding option "' padding '". use zeros or circ...']); 
        end

        [~,idx] = max2(C);    
        idx = -idx + floor(size(M_new)/2);

    end

    if use_minimizer
    
    if cs(padding, 'zeros')
        func = @(b) sum2((M_ref - b(3)*imshift(M_new, round(b(1), b(2)))).^2);
    elseif cs(padding, 'circ')
        func = @(b) sum2((M_ref - b(3)*circshift(M_new, round([b(1), b(2)]))).^2);
    else
        error(['unknown padding option "' padding '". use zeros or circ...']);        
    end
    
    b0 = [-idx(1) -idx(2) 1];
    
    b_found = fminsearch(func, b0);
    
    idx = -b_found(1:2);
    
    end
    
    idx
    M_shifted = imshift(M_new, idx(1), idx(2));
    
end