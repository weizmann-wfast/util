function [R, I] = radialExtent(I, sigma_clipping)

    import util.stat.sum2;

    if nargin<2 || isempty(sigma_clipping)
        sigma_clipping=0;
    end

    if sigma_clipping<=0
        I = sum(I,3,'double');
    end
    
    sumI = sum2(I);

    height = size(I,1);
    width = size(I,2);

    X = meshgrid((1:width), (1:height));
    Y = meshgrid((1:height), (1:width))';

    x1 = bsxfun(@rdivide, sum2(bsxfun(@times, X, I)), sumI);
    y1 = bsxfun(@rdivide, sum2(bsxfun(@times, Y, I)), sumI);

    R = bsxfun(@rdivide, sum2(bsxfun(@times, bsxfun(@minus,X,x1).^2+bsxfun(@minus,Y,y1).^2, abs(I))), sumI);

    if sigma_clipping>0
       mean_result = mean(R);
       bad_indices = find(R>sigma_clipping*mean_result);
       R(bad_indices) = [];
       R = mean(R);
       
       I(:,:,bad_indices) = [];
       I = mean(I,3);
       
    end
    
end