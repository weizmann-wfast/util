function [image_reduced, mask] = maskBadPixels(image, filler, sigma)

    import util.img.maskBadPixels;
    import util.stat.median2;
    
    if nargin<2
        filler = [];
    end
    
    if nargin<3
        sigma = [];
    end
    
    if size(image,4)>1
        
        image_reduced = zeros(size(image), 'like', image);
        mask = false(size(image));
        for ii = 1:size(image,4)
            [image_reduced(:,:,:,ii), mask(:,:,:,ii)] = maskBadPixels(image(:,:,:,ii), filler, sigma);
        end
        
        return;
        
    end
    
    if size(image,3)>1
        
        image_reduced = zeros(size(image), 'like', image);
        mask = false(size(image));
        for ii = 1:size(image,3)
            [image_reduced(:,:,ii), mask(:,:,ii)] = maskBadPixels(image(:,:,ii), filler, sigma);
        end
        
        return;
        
    end
    
    if isempty(filler)
        filler = median2(image);
    end

    if isempty(sigma)
        sigma = 3;
    end
    
    ker = ones(3)./8;
    ker(2,2) = 0; % nearest neighbors kernel
    image_conv = filter2(ker, double(image));
    
    mask = logical( (double(image))>abs(sigma.*(image_conv)) );
    
    image_reduced = image;
    image_reduced(mask) = filler;

end