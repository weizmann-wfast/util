function [dynamic_range_vector, image_reduced] = autodyn(image, filler)

    import util.img.maskBadPixels;
    import util.stat.max2;
    import util.stat.min2;

    if nargin<2 || isempty(filler)
        filler = [];
    end

    image_reduced = maskBadPixels(image, filler);

    ker = ones(3)./8;
    ker(2,2) = 0;
    
    image_reduced = conv2(double(image_reduced), ker, 'valid');
    
    dynamic_range_vector(1) = min2(image_reduced);
    dynamic_range_vector(2) = max2(image_reduced);
    
end