function I = gaussian2(CX, CY, rot_frac, S, use_negative)

    if nargin<2 || isempty(CY)
        CY = CX;
    end

    if isempty(CX) && ~isempty(CY)
        CX = CY;
    end
    
    if nargin<3 || isempty(rot_frac)
        rot_frac = 0;
    end
    
    if nargin<4 || isempty(S)
        S = ceil((max(CX, CY)*10));
        S = S+mod(S,2)+1;
    end
    
    if nargin<5 || isempty(use_negative)
        use_negative = 0;
    end
            
    [x,y] = meshgrid(-floor((S)/2):floor((S-1)/2));

    x2 = +x*cos(pi/2*rot_frac)+y*sin(pi/2*rot_frac);
    y2 = -x*sin(pi/2*rot_frac)+y*cos(pi/2*rot_frac);
    
    if ~use_negative
        I = exp(-0.5*((x2./CX).^2 + (y2./CY).^2));
    else
        I = exp(-0.5*(sign(CX).*(x2./CX).^2 + sign(CY).*(y2./CY).^2));
    end
    
end