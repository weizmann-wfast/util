function M_out = droptile(M_in, tile, position_yx)
% drops (adds) the tile to the input matrix M_in at position (y,x)
% can handle 3D matrices and expands the tile if it is 2D

    import util.img.imshift;
    import util.img.crop2size;
    
    if size(M_in,1)<=size(tile,1) && size(M_in,2)<=size(tile,2)
        tile = imshift(tile, position_yx(1)-size(M_in,1)/2, position_yx(2)-size(M_in,2)/2);
        M_out = bsxfun(@plus, M_in, crop2size(tile, [size(M_in,1) size(M_in,2)]));
        return;
    end
    
    x1 = round(position_yx(2))-floor(size(tile,2)/2);
    x2 = round(position_yx(2))+ceil(size(tile,2)/2)-1;
    y1 = round(position_yx(1))-floor(size(tile,1)/2);
    y2 = round(position_yx(1))+ceil(size(tile,1)/2)-1;
    
    if x2<1 || x1>size(M_in,2)
        M_out = M_in; 
        return;
    end
    
    if y2<1 || y1>size(M_in,1)
        M_out = M_in;
        return;
    end
    
    if x1<1 % if we are too far to the left
        tile = tile(:,2-x1:end,:);
        x1 = 1;
    end
    
    if x2>size(M_in, 2) % if we are too far to the right
        tile = tile(:, 1:end-(x2-size(M_in,2)),:);
        x2 = size(M_in,2);
    end
    
    if y1<1 % if we are too far to the top
        tile = tile(2-y1:end,:,:);
        y1 = 1;
    end
    
    if y2>size(M_in, 1) % if we are too far to the bottom
        tile = tile(1:end-(y2-size(M_in,1)),:);
        y2 = size(M_in,1);
    end
        
%     disp(['x= ' num2str(position_yx(2)) ' | y= ' num2str(position_yx(1)) ' | size(M_in)= ' num2str(size(M_in)) ' | size(tile)= ' num2str(size(tile))]);
    
    M_out = M_in;
    M_out(y1:y2, x1:x2, :) = bsxfun(@plus, M_out(y1:y2, x1:x2,:), tile);
    
end