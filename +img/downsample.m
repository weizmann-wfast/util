function I_out = downsample(I, binning, normalization, memory_limit)

    if nargin<2 || isempty(binning)
        binning = 2;
    end
    
    if nargin<3 || isempty(normalization)
        normalization = 'sum';
    end
    
    if nargin<4 || isempty(memory_limit)
        memory_limit = 10; % GBs
    end
    
    if binning==1
        I_out = I;
        return;
    end
    
    index = mod(size(I),binning)+1;
    
%     I_out = imresize(I(index(1):end, index(2):end, :), 1./binning);

    w = whos('I');
    if w.bytes/1024^3>memory_limit        
        if size(I,3)>1
            I_out = zeros(floor(size(I,1)./binning), floor(size(I,2)./binning), size(I,3), 'like', I);
            for ii = 1:size(I,3)
                I_out(:,:,ii) = util.img.downsample(I(:,:,ii), binning, normalization, memory_limit);
            end
        else
            error(['Not enough memory to hold the new array. Required ram: ' num2str(w.bytes/1024^3)]);
        end        
    end

    I_conv = zeros(size(I), 'like', I);
    k = ones(binning);
    
    if util.text.cs(normalization, 'mean')    
        k = k./sum(k(:));
    end
    
    for ii = 1:size(I,3)
        I_conv(:,:,ii) = filter2(k, I(:,:,ii)); 
    end
    
    I_out = I_conv(index(1):binning:end, index(2):binning:end, :);

end