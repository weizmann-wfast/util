function I_new = centering(I, varargin)
   
    import util.img.*;
    import util.stat.*;
    import util.text.*;
    
    if size(I,4)>1 % revert to loop if 4D
        I_new = zeros(size(I), 'like', I);
        for ii = 1:size(I,4)
            I_new(:,:,:,ii) = centering(I(:,:,:,ii), varargin{:});
        end
        return;
    end
    
    if size(I,3)>1 % revert to loop if 3D
        I_new = zeros(size(I), 'like', I);
        for ii = 1:size(I,3)
            I_new(:,:,ii) = centering(I(:,:,ii), varargin{:});
        end
        return;
    end
    
    % must be a 2D matrix
    
    finder = 'max'; % can also choose moment
    shift_type = 'zeros'; % can also choose circ
    bad_pixels = 1; % use maskBadPixels
    smoothing = 0;
    subtraction = 'none'; % can choose median or corner
    
    for ii = 1:2:length(varargin)
        
        key = varargin{ii};
        val = varargin{ii+1};
        
        if cs(key, {'finder', 'peak'})
            finder = val;
        elseif cs(key, 'shift_type')
            shift_type = val;
        elseif cs(key, {'pixels', 'bad_pixels'})
            bad_pixels = parse_bool(val);
        elseif cs(key, 'smoothing')
            smoothing = val;
        elseif cs(key, 'subtraction')
            subtraction = val;
        end
        
    end
    
    I_finder = I;
    S = size(I_finder);
        
    if bad_pixels
        I_finder = maskBadPixels(I_finder);
    end
    
    if cs(subtraction, 'median')
        I_finder = I_finder - median2(I_finder);
    elseif cs(subtraction, 'corner')
        I_finder = I_finder - corner_mean(I_finder);
    elseif cs(subtraction, 'none')
        
    else
        error(['unknown subtraction method: "' subtraction '", use median, corner or none...']);
    end
    
    if smoothing
        I_finder = filter2(gaussian2(smoothing), I_finder, 'same');
    end
    
    if cs(finder, 'max')
        
        [~, idx] = max2(I_finder);
        shift_x = round(-idx(2) + S(2)/2);
        shift_y = round(-idx(1) + S(1)/2);
        
    elseif cs(finder, 'moments')
        
        [x,y] = meshgrid((1:S(1)) - S(1)/2, (1:S(2)) - S(2)/2);
        shift_x = -sum2(I_finder.*x)./sum2(I_finder);
        shift_y = -sum2(I_finder.*y)./sum2(I_finder);
        
    else
        error(['unknown finder method: "' finder '", use max or moments...']);      
    end
    
    if cs(shift_type, 'zeros')
        I_new = imshift(I, shift_y, shift_x);
    elseif cs(shift_type, 'circ')
        I_new = circshift(I, [shift_y, shift_x]);
    else
        error(['unknown shift type: "' shift_type '", use zeros or circ...']);
    end
    
end