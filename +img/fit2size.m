function M_out = fit2size(M_in, S_needed)
% pads or crops the given array M_in to size M_size, putting it in the middle. Won't
% shrink the array... can handle 3D matrices (expands only first 2 dims)
    
    import util.img.*;

    M_out = crop2size(pad2size(M_in, S_needed), S_needed);
    
end