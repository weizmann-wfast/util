function M_out = pad2size(M_in, S_needed)
% pads the given array M_in to size M_size, putting it in the middle. Won't
% shrink the array... can handle 3D matrices (expands only first 2 dims)
    
    if isscalar(S_needed)
        S_needed = [1 1]*S_needed;
    else
        S_needed = S_needed(1:2);
    end
    
    S_in = size(M_in);
    S_in = S_in(1:2);

    if S_needed(1)>S_in(1) || S_needed(2)>S_in(2)
        
        M_out = zeros(S_needed(1), S_needed(2), size(M_in,3));

        gap = (S_needed-S_in)/2;
        
        y1 = 1+ceil(gap(1));
        y2 = S_needed(1)-floor(gap(1));
        x1 = 1+ceil(gap(2));
        x2 = S_needed(2)-floor(gap(2));
        
        M_out(y1:y2, x1:x2,:) = M_in;
        
    else
        M_out = M_in;
    end
    
end