% taken shamelessly from https://www.mathworks.com/matlabcentral/newsreader/view_thread/295157
% and also added a cancel button

function [R] = inputdlg(St,D)
% Input dialog. User may hit return after entering info.

if nargin<2 % Default values if called without args.
    D = '5';
elseif nargin<1
    St = 'myinput';
end

if ~ischar(D)
    D = num2str(D);
end

R = []; % In case the user closes the GUI.
S.fh = figure('units','pixels',...
              'position',[50 500 200 100],...
              'menubar','none',...
              'numbertitle','off',...
              'name',St,...
              'resize','off');
S.ed = uicontrol('style','edit',...
                 'units','pix',...
                'position',[10 60 180 30],...
                'string',D);
S.pb = uicontrol('style','pushbutton',...
                 'units','pix',...
                'position',[10 20 90 30],...
                'string','Enter',...
                'callback',{@pb_call});
S.cn = uicontrol('Style', 'pushbutton',...
                'units', 'pix',...
                'position',[100 20 90 30],...
                'string', 'Cancel',...
                'callback',{@cn_call});
set(S.ed,'call',@ed_call)
uicontrol(S.ed) % Make the editbox active.
uiwait(S.fh) % Prevent all other processes from starting until closed.

    function [] = pb_call(varargin)
%         R = str2double(get(S.ed,'string'));
        R = get(S.ed,'String');
        close(S.fh); % Closes the GUI, allows the new R to be returned.
    end

    function [] = ed_call(varargin)
        uicontrol(S.pb)
        drawnow
%         R = str2double(get(S.ed,'string'));
        if isvalid(S.ed)
            R = get(S.ed, 'String'); 
        else
            R = [];
        end
        close(gcbf)
    end
    
    function [] = cn_call(varargin)
        R = [];
        close(gcbf);
    end

end