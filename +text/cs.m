function a = cs(str, cell_array, num_letters)

if isempty(str) && iscell(cell_array) && ~any(cellfun(@isempty, cell_array))
    a = 0;
    return;
end

if isempty(str) &&  ~isempty(cell_array)
    a = 0;
    return;
end

str = strrep(str, '_','');
str = strrep(str, ' ','');

if iscell(cell_array)
    for ii = 1:length(cell_array)
        cell_array{ii} = strrep(cell_array{ii}, '_','');
        cell_array{ii} = strrep(cell_array{ii}, ' ','');
    end
else
    cell_array = strrep(cell_array,'_','');    
    cell_array = strrep(cell_array,' ','');
end

if nargin<3 || isempty(num_letters) || num_letters==0
    num_letters = length(str);
end

a = any(strncmpi(str, cell_array, num_letters));

end