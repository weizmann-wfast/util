function str = sa(varargin)
% shortcut function from the evidently ever so useful slash_append. 

    str = util.text.slash_append(varargin{:});

end