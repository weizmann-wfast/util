function out_filename = extension(input_filename, new_ext)
% overrides the extension of a filename

    [path, filename] = fileparts(input_filename);
    
    if new_ext(1)~='.'
        new_ext = ['.' new_ext];
    end
    
    out_filename = fullfile(path, [filename, new_ext]);
    
end