function str = f2s(float, decimals)

    if nargin<2 || isempty(decimals)
        decimals = 2;
    end
    
    str = '';
    
    for ii = 1:length(float)
    
        form = ['%4.' num2str(floor(decimals))];
        if float==0
            str = '0';
        elseif abs(float(ii))<1000 && abs(float(ii))>0.1
            form = [form 'f '];
        else
            form = [form 'e '];
        end

        str = [str sprintf(form, float(ii))];
        
    end

end