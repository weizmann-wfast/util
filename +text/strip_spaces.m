function str_out = strip_spaces(str_in)

    str_out = strrep(str_in, '_','');
    str_out = strrep(str_out, ' ','');
    
end