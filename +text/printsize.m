function s = printsize(M, M2)

    if nargin==1
        s = ['size(' inputname(1) ')= ' num2str(size(M))];
    elseif nargin==2
        s = ['size(' inputname(1) ')= ' num2str(size(M)) ' | size(' inputname(2) ')= ' num2str(size(M2))];
    end
    
end