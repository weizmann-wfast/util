function val = parse_bool(input)

    if isnumeric(input) || islogical(input)
        val = input;
        return;
    end
    
    val = str2num(input);
    if ~isempty(val)
        return;
    end
    
    if util.text.cs(input, {'yes','on'})
        val = 1;
    else
        val = 0;
    end
    

end