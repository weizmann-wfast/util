function time = str2time(str)
% parses a time string (FITS format compliant) into a datetime object   

    vec = sscanf(str, '%4d-%2d-%2dT%2d:%2d:%f')';

    time = datetime(vec);
    
end