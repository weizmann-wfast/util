function str = n2s(number, min_prec, max_prec)

    if nargin<2 || isempty(min_prec)
        min_prec = 0;
    end
    
    if nargin<3 || isempty(max_prec)
        max_prec = Inf;
    end
    
    if max_prec<min_prec
        max_prec=min_prec;
    end
    
    if all(floor(number)==number) && min_prec==0 || max_prec==0
        str = num2str(number, '%d ');
    elseif all(floor(number*10)==number*10) && min_prec<=1 || max_prec==1
        str = num2str(number, '%3.1f ');
    elseif all(floor(number*100)==number*100) && min_prec<=2 || max_prec==2
        str = num2str(number, '%4.2f ');        
    elseif all(floor(number*1000)==number*1000) && min_prec<=3 || max_prec>=3
        str = num2str(number, '%5.3f ');
    elseif all(floor(number*1e4)==number*1e4) && min_prec<=4 || max_prec>=4
        str = num2str(number, '%6.4f ');
    elseif all(floor(number*1e5)==number*1e5) && min_prec<=5 || max_prec>=5
        str = num2str(number, '%7.5f ');
    elseif all(floor(number*1e6)==number*1e6) && min_prec<=6 || max_prec>=6
        str = num2str(number, '%8.6f ');        
    elseif all(floor(number*1e7)==number*1e7) && min_prec<=7 || max_prec>=7
        str = num2str(number, '%7.7f ');
    end

end