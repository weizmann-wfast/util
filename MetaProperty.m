classdef MetaProperty
% list item for MetaData. 
%
% TEST PROTOCOL: p=util.MetaProperty;

    properties
            
        name@char;
        type@char  = 'data'; % can also be 'att' or 'attribute' or 'object'
        alias = {}; % where to save inside the HDF5 file
        default_value;
        dependent = 1; % if 1, don't bother reading from file
        hidden    = 0; % if 1, don't display
        transient = 0; % if 1, don't save/load at all!
        copyable  = 1; % if 0, don't copy! 
        external  = 0; % if 1, don't delete or reset (save/load??)
                
        
    end
        
    methods % constructor
       
        function obj = MetaProperty(name, type, alias, default_value, hidden, dependent, transient, copyable, external)
            
%             disp('MetaProperty constructor');
            
            if nargin<2 || isempty(type)
                type = 'data';
            end
            
            if nargin<3 || isempty(alias)
                alias = {};
            end
            
            if nargin<4 || isempty(default_value)
                default_value = [];
            end
            
            if nargin<5 || isempty(hidden)
                hidden = 0;
            end
            
            if nargin<6 || isempty(dependent)
                dependent = 0;
            end
            
            if nargin<7 || isempty(transient)
                transient = 0;
            end
            
            if nargin<8 || isempty(copyable)
                copyable = 1;
            end
            
            if nargin<9 || isempty(external)
                external = 0;
            end
            
            obj.name = name;
            obj.type = type;
            obj.alias = alias;
            obj.default_value = default_value;
            obj.dependent = dependent;
            obj.hidden = hidden;
            obj.transient = transient;
            obj.copyable = copyable;
            obj.external = external;
%             obj.name
            
        end
        
    end
    
end