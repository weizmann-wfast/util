classdef ExpandingArray < handle
   
    properties
       
        full_matrix;
        exp_dim;
        current_size=0;
        
    end
    
    properties(Dependent=true)
             
        data;
        capacity;
        memory;
        used;        
    end
    
    methods % constructor
       
        function obj = ExpandingArray(data, dim, capacity)
        
            if nargin==1 && isa(data, util.ExpandingArray)
                
                obj.full_matrix = data.full_matrix;
                obj.exp_dim = data.exp_dim;
                obj.current_size = data.size;
            
            else
                
                if nargin<2 || isempty(dim)
                    dim = 1;
                end
                
                if nargin<3 || isempty(capacity)
                    capacity = size(data, dim);
                end
                
                obj.exp_dim = dim;
                
                size_vec = size(data);
                
                size_vec(dim) = capacity;
                
                obj.full_matrix = nan(size_vec);
                
                obj.data = data;
                            
            end
            
            
        end
                
    end
    
    methods % getters
       
        function str = subs_str(obj, start_val, end_val)
           
            if isnumeric(start_val)
                start_val = num2str(start_val);
            end
            
            if isnumeric(end_val)
                end_val = num2str(end_val);
            end
            
            if strcmp(end_val, 'end')
                end_val = num2str(size(obj, obj.exp_dim));
            end
            
            str = '';
            
            for ii = 1:obj.exp_dim-1
               
                str = [str ':,'];
                
            end
            
            str = [str start_val ':' end_val];
            
            for ii = obj.exp_dim+1:obj.ndims
               
                str = [str ',:'];
                
            end
            
        end
        
        function C = subs_cell(obj, value)
                        
            C = cell(1, obj.ndims);
            C(:) = {':'};
            C{obj.exp_dim} = value;
            
        end
        
        function s = subs_struct(obj, value)
            
            s.type = '()';
            s.subs = obj.subs_cell(value);
            
        end
        
        function d = get.data(obj)
            
           d = subsref(obj.full_matrix, obj.subs_struct(1:obj.current_size));
            
        end
       
        function c = get.capacity(obj)
           
            c = size(obj.full_matrix, obj.exp_dim);
            
        end
        
        function d = ndims(obj)
           
            d = ndims(obj.full_matrix);
            
        end
        
        function m = get.memory(obj)
           
            m = prod(size(obj.full_matrix)).*8;
            
        end
        
        function m = get.used(obj)
            
            m = prod(size(obj)).*8;
            
        end
        
    end
    
    methods % setters
       
        function obj = set.capacity(obj, new_cap)
           
            if new_cap<=obj.capacity
                return;
            end
            
            size_vec = size(obj.full_matrix);
            size_vec(obj.exp_dim) = new_cap-obj.capacity;
            
            obj.full_matrix = cat(obj.exp_dim, obj.full_matrix, nan(size_vec));
            
        end
        
        function obj = set.data(obj, input)
           
            if ndims(input)<obj.exp_dim
                error(['input array is lower than exp_dim= ' num2str(obj.exp_dim)]);
            end
            
            input_size_vec = size(input);
            input_size_vec(obj.exp_dim) = 0;
            data_size_vec = size(obj.full_matrix);
            data_size_vec(obj.exp_dim) = 0;
                        
            new_size = size(input, obj.exp_dim);
            
            if new_size >= obj.capacity
            
                obj.full_matrix = input;
                obj.current_size = new_size;
                
            elseif length(input_size_vec)~=length(data_size_vec) || any(input_size_vec~=data_size_vec)
                
                input_size_vec(obj.exp_dim) = obj.capacity;
                obj.full_matrix = nan(input_size_vec);
%                 obj.full_matrix = subsasgn(obj.full_matrix, obj.subs_struct(1:new_size), input);
                str = sprintf('obj.full_matrix(%s) = input;', obj.subs_str(1, new_size));
                eval(str);
                obj.current_size = new_size;
%                 error(['size mismatch. data= ' num2str(size(obj.data)) ' | input= ' num2str(size(input))]); 
                               
            else
                str = sprintf('obj.full_matrix(%s) = input;', obj.subs_str(1, new_size));
                eval(str);
%                 obj.full_matrix = subsasgn(obj.full_matrix, obj.subs_struct(1:new_size), input);
                obj.current_size = new_size;
                
            end
            
        end
        
    end
    
    methods % overloads! 
       
        function m = double(obj)
            
            m = obj.data;
            
        end
        
        function m = plus(obj1, obj2)
            
            m = double(obj1)+double(obj2);
            
        end
        
        function m = uplus(obj)
            
            m = obj;
            
        end
        
        function m = minus(obj1, obj2)
            
            m = double(obj1)-double(obj2);
            
        end
        
        function m = uminus(obj)
            
            obj.data = -obj.data;
            
            m = obj;
            
        end
                
        function m = times(obj1, obj2)
                    
            m = double(obj1).*double(obj2);
            
        end
        
        function m = rdivide(obj1, obj2)
            
            m = double(obj1)./double(obj2);
            
        end
        
        function m = power(obj1, obj2)
            
            m = double(obj1).^double(obj2);
            
        end
            
        function m = lt(obj1, obj2)
            
            m = double(obj1)<double(obj2);
            
        end
        
        function m = gt(obj1, obj2)
            
            m = double(obj1)>double(obj2);
            
        end
        
        function m = le(obj1, obj2)
            
            m = double(obj1)<=double(obj2);
            
        end
        
        function m = ge(obj1, obj2)
            
            m = double(obj1)>=double(obj2);
            
        end
        
        function m = ne(obj1, obj2)
            
            m = double(obj1)~=double(obj2);
            
        end
        
        function m = eq(obj1, obj2)
            
            m = double(obj1)==double(obj2);
            
        end
        
        function m = and(obj1, obj2)
                        
            m = double(obj1) & double(obj2);
            
        end
        
        function m = or(obj1, obj2)
            
            m = double(obj1) | double(obj2);
            
        end
        
        function obj = not(obj)
            
            obj.data = ~obj.data;
            
        end
        
        function m = transpose(obj)
            
            m = permute(obj, [2,1]);
            
        end
        
        function m = ctranspose(obj)
        
            m = obj.';
            
        end
        
        function obj = permute(obj, dims)
           
            if length(dims)<obj.ndims
                error('length of dims must be at least the number of dimensions of array!');
            end
            
            obj.exp_dim = find(dims==obj.exp_dim);
            
            obj.full_matrix = permute(obj.full_matrix, dims);
            
        end
        
        function obj1 = cat(dim, obj1, obj2)
            
            obj2 = double(obj2);
            
            if dim==obj1.exp_dim
                
                size_v1 = size(obj1);
                size_v1(dim) = 0;
                size_v2 = size(obj2);
                size_v2(dim) = 0;
                
                assert(length(size_v1)==length(size_v2) && all(size_v1==size_v2), ['size mismatch, size(obj1)= ' num2str(size_v1) ' | size(obj2)= ' num2str(size_v2)]); 
                               
                new_size=size(obj2,dim)+size(obj1, dim);
                
                if new_size>=obj1.capacity
                    obj1.full_matrix = cat(dim, obj1.data, obj2);
                    obj1.current_size = new_size;
                else
%                     obj1.data = subsasgn(obj1.data, obj1.subs_struct(size(obj1)+1:new_size), obj2);
                    
                    str = sprintf('obj1.full_matrix(%s) = obj2;', obj1.subs_str(obj1.current_size+1, new_size));
                    eval(str);
                    
                    obj1.current_size = new_size;
                end
                
            else
                obj1.data = cat(dim, obj1.data, obj2);
            end
            
        end
        
        function obj1 = horzcat(obj1, obj2)
            
            obj1 = cat(2, obj1, obj2);
            
        end
        
        function obj1 = vertcat(obj1, obj2)
            
            obj1 = cat(1, obj1, obj2);
            
        end

        function n = numArgumentsFromSubscript(obj,s,indexingContext)

           n = 1;
           
       end
        
        function varargout = subsref(obj,s)
         % obj(i) is equivalent to obj.data(i)
         
         switch s(1).type
            case '.'
               varargout{1} = builtin('subsref',obj,s);
            case '()'
               if length(s)<2
                  varargout{1} = builtin('subsref',obj.data,s);
               else
                  varargout = builtin('subsref',obj,s);
               end
            case '{}'
               error('Not a supported subscripted reference')
         end
                  
      end
      
        function obj = subsasgn(obj,s,val)
         if isempty(s) && isa(val,'util.ExpandingArray')
            obj = util.ExpandingArray(val);
         end
         switch s(1).type
            case '.'
               obj = builtin('subsasgn',obj,s,val);
            case '()'
               %
               if length(s)<2
                  if isa(val,'util.ExpandingArray')
                     error('Object must be scalar')
                  elseif isa(val,'double')
                     snew = substruct('.','data','()',s(1).subs(:));
                     obj = subsasgn(obj,snew,val);
                  end
               end
            case '{}'
               error('Not a supported subscripted assignment')
         end
      end
        
        function m = empty(obj)
            
            m = builtin('empty', obj.data);
            
        end
        
        function m = numel(obj)
            
            m = builtin('numel', obj.data);
            
        end
        
        function m = size(obj, dim)
             
            m = size(obj.full_matrix);
            
            m(obj.exp_dim) = obj.current_size;
            
            if nargin>1 && ~isempty(dim)
                m = m(dim);
            end
                       
        end
        
        function m = length(obj)
        
            m = builtin('length', obj.data);
            
        end
      
      function ind = end(obj,k,n)
         szd = size(obj.data);
         if k < n
            ind = szd(k);
         else
            ind = prod(szd(k:end));
         end
      end
            
    end
    
end