function I_f = fft2s(I)

    import util.fft.fftshift2;

    I_f = fftshift2(fft2(fftshift2(I)));

end