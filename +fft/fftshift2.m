function Ishift = fftshift2(I)

    Ishift = fftshift(fftshift(I,1),2);

end