function I_f = ifft2s(I)

    import util.fft.fftshift2;

    I_f = fftshift2(ifft2(fftshift2(I)));

end